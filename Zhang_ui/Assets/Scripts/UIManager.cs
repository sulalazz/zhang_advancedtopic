﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Animator dialog;
    public Animator contentPanel;
    bool Open;
    public Image image;
    public GameObject[] panels;
    int page = 0;
    public Animator pages;
    public Animator startButton;
    public Animator mapsButton;
    public Animator startPanel;

    public GameObject elementaryPanel;
    public GameObject intermediatePanel;
    public GameObject advancedPanel;

    public void TogglePanel ()
    {

        Open = !Open;
        
        if(Open)
        {
            dialog.SetBool("isHidden", false);
        }
        else 
        {
            dialog.SetBool("isHidden", true);
        }
    }

    public void ToggleMenu()
    {
        bool isHidden = contentPanel.GetBool("isHidden");
        contentPanel.SetBool("isHidden", !isHidden);
    }

    public void brigtnessSet (float sliderValue)
    {
        float alpha = 1f - sliderValue;
        alpha = alpha * 0.4f;
        image.color = new Color(0f, 0f, 0f, alpha);

    }

    public void changePage(int change)
    {
        //print(change);
        panels[page].SetActive(false);

        page += change;

        if(page<0)
        {
            page = panels.Length + page;

            
        }

        page = page % panels.Length;

        panels[page].SetActive(true);
    }

    public void OpenSettings()
    {
        pages.SetBool("isHidden", false);

        mapsButton.SetBool("isHidden", true);
    }

    public void CloseSetting()
    {
        pages.SetBool("isHidden", true);
        //startButton.SetBool("isHidden", false);
        mapsButton.SetBool("isHidden", false);
    }

    public void OpenStart()
    {
        startButton.SetBool("isHidden", true);
        startPanel.SetBool("isHidden", false);
    }

    public void CloseStart()
    {
        startButton.SetBool("isHidden", false);
        startPanel.SetBool("isHidden", true);
    }

    public void OpenElementaryPanel()
    {
        if (startPanel != null)
        {
            elementaryPanel.SetActive(true);
        }
    }

    public void OpenIntermediatePanel()
    {
        if (startPanel != null)
        {
            intermediatePanel.SetActive(true);
        }
    }

    public void OpenAdvancedPanel()
    {
        if (startPanel != null)
        {
            advancedPanel.SetActive(true);
        }
    }

    public void CloseElementaryPanel()
    {
        elementaryPanel.SetActive(false);
    }

    public void CloseIntermediatePanel()
    {
        intermediatePanel.SetActive(false);
    }

    public void CloseAdvancedPanel()
    {
        advancedPanel.SetActive(false);
    }
}
